# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 22:11:06 2020

@author: Bljeona
"""
#Zad2
class NumberNotInRangeError(Exception):
    
    # def _init_(self,x, message="Nije u intervalu"):
    #     self.x=x
    #     self.message=message
    #     super()._init_(self.message)
        
    pass


while True :
    try:
        x = float (input ("Unesite ocjenu: "))
        if not 0 < x <1:
            raise NumberNotInRangeError(x)
        if  x >= 0.9:
            print("A")
            break
        if  x >= 0.8:
            print("B")
            break
        if x >= 0.7:
            print("C")
            break
        if x >= 0.6:
            print("D")
            break
        if x < 0.6:
            print("F")
            break
        break 
    except NumberNotInRangeError:
        print("Nije u intervalu")
    except ValueError:
        print("Unesite broj")