import numpy as np
import matplotlib.pyplot as plt

throw = np.random.randint(1, 7, 100)
bins = np.linspace(1, 6, 20)
plt.hist(throw, bins, color='pink')
plt.show()
