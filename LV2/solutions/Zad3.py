import numpy as np
import matplotlib.pyplot as plt

mw = np.random.randint(2,size=180)
heights = np.ones(180,dtype=int)

heights[mw == 0] = np.random.normal(167,7,len(heights[mw == 0]))
heights[mw == 1] = np.random.normal(180,7,len(heights[mw == 1]))
plt.hist(heights[mw == 0],bins = 80,color = 'red')
plt.hist(heights[mw == 1],bins = 80,color = 'blue')

def avg_height_w():
    return sum(heights[mw == 0]/len(heights[mw == 0]))
def avg_height_m():
    return sum(heights[mw == 1]/len(heights[mw == 1]))

plt.axvline(avg_height_w(), color='orange')
plt.axvline(avg_height_m(), color='green')
plt.legend(["Average_w","Average_m","W","M"])

