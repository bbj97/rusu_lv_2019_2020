import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('../resources/tiger.png')
brightness =8
modified = np.multiply(img, brightness)
modified = np.clip(modified, 0, 1)
imgplot = plt.imshow(modified) 

plt.show() 

