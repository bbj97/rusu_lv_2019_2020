# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 22:58:47 2020

@author: Bljeona
"""

#Zadatak1a
import pandas as pd

cars=pd.read_csv('C:/Users/student/Desktop/LV3_resources_mtcars.csv')
carsDes = cars.sort_values(by=['mpg'], ascending=False)
print(carsDes.head(5))

#Zadatak1b
eightCyl = cars.query("cyl==8")
carsAsc = eightCyl.sort_values(by=['mpg'], ascending=True)
print(carsAsc.head(5))

#Zadatak1c
sixCyl = cars.query("cyl==6")
avgMpgS = sixCyl["mpg"].mean()
print(avgMpgS)

#Zadatak1d
fourCylS=cars.query("cyl==4")
fourCyl = fourCylS.query("wt>=2.000 & wt<=2.200")
avgMpgF=fourCyl["mpg"].mean()
print(avgMpgF)

#Zadatak1e
amAuto = cars.query("am==0")
amManu = cars.query("am==1")
print(amAuto.shape[0])
print(amManu.shape[0])

#Zadatak1f
amAuto = cars.query("am==0")
AutoHp = amAuto.query("hp>100")
print(AutoHp.shape[0])

#Zadatak1g
print(cars.wt*1000)