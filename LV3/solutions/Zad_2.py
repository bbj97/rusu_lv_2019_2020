# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 23:27:12 2020

@author: Bljeona
"""

#Zadatak2a
import pandas as pd
import matplotlib.pyplot as plt

cars=pd.read_csv('C:/Users/student/Desktop/LV3_resources_mtcars.csv')
# four=(cars.query('cyl == 4').mpg).mean()
# six=(cars.query('cyl == 6').mpg).mean()
# eight=(cars.query('cyl == 8').mpg).mean()

# cars = {'4':four, '6':six, '8':eight} 

# cylinders = list(cars.keys()) 
# values = list(cars.values()) 

# fig = plt.figure(figsize = (6, 6)) 

# plt.bar(cylinders, values, color ='#104E8B',width=0.3) 

# plt.xlabel("Broj cilindara") 
# plt.ylabel("Prosječna potrošnja") 
# plt.title("Prosječna potrošnja automobila prema broju cilindara") 
# plt.show()

#Zadatak2b
# four=(cars.query('cyl == 4').wt).mean()
# six=(cars.query('cyl == 6').wt).mean()
# eight=(cars.query('cyl == 8').wt).mean()
# cars = {'4':four, '6':six, '8':eight} 
# cylinders = list(cars.keys()) 
# values = list(cars.values()) 
# fig = plt.figure(figsize = (6, 6)) 
# plt.bar(cylinders, values, color ='#104E8B',width=0.3)
# plt.show()

#Zadatak2c
# amAuto=(cars.query("am==1").mpg).mean()
# amManu=(cars.query("am==0").mpg).mean()


# podaci = {'RUCNI':amManu, 'AUTOMATSKI':amAuto} 

# mjenjac = list(podaci.keys()) 
# values = list(podaci.values()) 

# fig = plt.figure(figsize = (6, 6)) 

# plt.bar(mjenjac, values, color ='#104E8B', width = 0.2) 

# plt.xlabel("Mjenjac") 
# plt.ylabel("Prosječna potrošnja") 
# plt.title("Prosječna potrošnja automobila ovisno o mjenjacu") 
# plt.show() 

#Zadatak2d
plt.scatter(cars[cars.am == 0]
            ['qsec'], cars[cars.am == 0]
            ['hp'], label = 'Automatski', s = cars[cars.am == 0]
            ['wt'] * 20)
plt.scatter(cars[cars.am == 1]
            ['qsec'], cars[cars.am == 1]
            ['hp'], label = 'Rucni', s = cars[cars.am == 1]
            ['wt'] * 20)
plt.legend(loc = 1)
plt.show()