# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 13:44:46 2021

@author: Bljeona
"""

from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                                   centers = 4,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

data = generate_data(50, 5)


link = linkage(data, 'complete')
plt.figure()
dn = dendrogram(link)
plt.show()

link = linkage(data, 'weighted')
plt.figure()
dn = dendrogram(link)
plt.show()

link = linkage(data, 'ward')
plt.figure()
dn = dendrogram(link)

link = linkage(data, 'centroid')
plt.figure()
#plt.savefig("C:\\Users\\Bljeona\\Documents\\rusu_lv_2019_2020\\LV5\\resources\\centroid.jpg")
dn = dendrogram(link)

link = linkage(data, 'median')
plt.figure()
dn = dendrogram(link)

link = linkage(data, 'single')
plt.figure()
dn = dendrogram(link)

link = linkage(data, 'average')
plt.figure()
dn = dendrogram(link)
