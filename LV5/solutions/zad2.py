# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 13:44:12 2021

@author: Bljeona
"""


from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from sklearn import cluster


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          centers=4,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

data = generate_data(500,5)

sse = []

for n_clusters in range(1, 21):
    kmeans = cluster.KMeans(init="random", n_clusters=n_clusters, 
                            n_init=10, max_iter=300, random_state=50)
    kmeans.fit(data)
    label = kmeans.fit_predict(data)
    sc = kmeans.inertia_
    sse.append(sc)
    
plt.plot(range(1, 21), sse)
plt.show()
